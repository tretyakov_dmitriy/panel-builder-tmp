<?php

namespace Digitech\PanelBuilder\Widgets;

abstract class ValueWidget extends Widget
{
    static $lessIsBetter = false;

    const VALUE_TYPE_CURRENCY = 'currency';
    const VALUE_TYPE_PERCENT = 'percent';
    const VALUE_TYPE_NUMBER = 'number';

    function info(): string
    {
        return '';
    }

    abstract function valueType(): string;

    abstract function currentValue();

    function currentLabel(): string
    {
        return '';
    }

    function prevLabel(): string
    {
        return '';
    }

    function prevValue()
    {
        return null;
    }

//    abstract static function chart(): Chart;

    public function payload()
    {
        return [
            'current_value' => $this->currentValue(),
            'prev_value' => $this->prevValue(),
            'current_label' => $this->currentLabel(),
            'prev_label' => $this->prevLabel(),
            'info' => $this->info(),
            'less_is_better' => static::$lessIsBetter,
            'value_type' => $this->valueType()
//            'chart' => static::chart()->serialize()
        ];
    }


    public function serialize()
    {
        $config = [
            'type' => 'value',
        ];
        return array_merge(
            $config,
            parent::serialize()
        );
    }
}
