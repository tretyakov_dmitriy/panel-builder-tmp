<?php

namespace Digitech\PanelBuilder\Widgets;

use Digitech\PanelBuilder\Views\CardView;

abstract class ResourceElementCardsCarouselWidget extends Widget
{
    abstract function resource(): string;

    function scope(): array {
        return []; // GET параметры, которые добавляются к ссылке при получении данных
    }

    function path(): string {
        return $this->resource()::getPath();
    }

    function view(): CardView {
        return $this->resource()::indexPage();
    }

    function navigateOnClick() {
        return null;
    }

    function serialize() {
        $config = [
            'type' => 'resource_element_cards_carousel',
            'resourceId' => $this->resource(),
            'path' => $this->path(),
            'view' => $this->view()->serialize(),
            'scope' => $this->scope(),
            'navigate_on_click' => $this->navigateOnClick()
        ];

        return array_merge($config, parent::serialize());
    }

}
