<?php

namespace Digitech\PanelBuilder\Widgets;

abstract class Widget
{
    abstract function authorize(): bool;

    function tooltip(): string {
        return '';
    }

    abstract function name(): string;

    function payload() {
        return [];
    }

    function serialize() {
        $config = [
            'id' => static::class,
            'name' => $this->name(),
            'tooltip' => $this->tooltip()
        ];

        return $config;
    }
}
