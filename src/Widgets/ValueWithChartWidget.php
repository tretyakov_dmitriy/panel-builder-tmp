<?php

namespace Digitech\PanelBuilder\Widgets;

use Digitech\PanelBuilder\Charts\Chart;

abstract class ValueWithChartWidget extends ValueWidget
{
    abstract function chart(): Chart;

    public function payload()
    {
        return array_merge([
            'chart' => $this->chart()->serialize(),
        ], parent::payload());
    }

    public function serialize()
    {
        return array_merge(
            parent::serialize(),
            [
                'type' => 'value_with_chart'
            ]
        );
    }
}
