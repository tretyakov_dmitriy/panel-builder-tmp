<?php

namespace Digitech\PanelBuilder\Widgets;

use Digitech\PanelBuilder\Views\CardView;

abstract class ResourceElementCardsListWidget extends Widget
{
    abstract function resource(): string;

    function scope(): array {
        return []; // GET параметры, которые добавляются к ссылке при получении данных
    }

    function path(): string {
        return $this->resource()::getPath();
    }

    function view(): CardView {
        return $this->resource()::indexPage();
    }

    function serialize() {
        $config = [
            'type' => 'resource_element_cards_list',
            'resourceId' => $this->resource(),
            'path' => $this->path(),
            'view' => $this->view()->serialize(),
            'scope' => $this->scope()
        ];

        return array_merge($config, parent::serialize());
    }

}
