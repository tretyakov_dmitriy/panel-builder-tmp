<?php

namespace Digitech\PanelBuilder\Widgets;

abstract class ActionsWidget extends Widget
{
    abstract function actions();

    function description() {
        return '';
    }
    public function serialize()
    {
        $actions_data = [];

        foreach (static::actions() as $label => $element) {
            $actions_data[] = $element::serialize(is_string($label) ? $label : null);
        }

        $config = [
            'type' => 'actions',
            'description' => $this->description(),
            'actions' => $actions_data,
        ];
        return array_merge(
            $config,
            parent::serialize()
        );
    }
}
