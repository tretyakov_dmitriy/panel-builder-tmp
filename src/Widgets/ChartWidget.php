<?php

namespace Digitech\PanelBuilder\Widgets;

use Digitech\PanelBuilder\Charts\Chart;

abstract class ChartWidget extends Widget
{
    const VALUE_TYPE_CURRENCY = 'currency';
    const VALUE_TYPE_PERCENT = 'percent';
    const VALUE_TYPE_NUMBER = 'number';

    function info() {
        return '';
    }

    abstract function valueType(): string;

    abstract function chart(): Chart;

    public function payload()
    {
        return [
            'chart' => $this->chart()->serialize(),
            'value_type' => $this->valueType()
        ];
    }

    public function serialize()
    {
        $config = [
            'type' => 'chart',
            'info' => $this->info(),
        ];
        return array_merge(
            $config,
            parent::serialize()
        );
    }
}
