<?php

namespace Digitech\PanelBuilder\Widgets;

use Digitech\PanelBuilder\Views\CardView;

abstract class ResourceElementFeedWidget extends ResourceElementCardsCarouselWidget
{

    function serialize() {
        $config = parent::serialize();
        $config['type'] = 'resource_element_feed';
        return $config;
    }

}
