<?php

namespace Digitech\PanelBuilder\Widgets;

abstract class AdvertisementWidget extends Widget
{
    public function payload()
    {
        return [
            'info' => $this->info(),
            'source_url' => 'https://i.ibb.co/XWmjCxG/800-1600.jpg',
            'target_url' => 'https://glazov.city/objects/4'
        ];
    }

    function info(): string
    {
        return '';
    }


    public function serialize()
    {
        $config = [
            'type' => 'advertisement',
        ];
        return array_merge(
            $config,
            parent::serialize()
        );
    }
}
