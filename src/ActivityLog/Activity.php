<?php

namespace Digitech\PanelBuilder\ActivityLog;

use Digitech\PanelBuilder\Traits\HasActions;

class Activity implements \JsonSerializable
{
    use HasActions;
    private $model, $user, $activity_classname;
    public function __construct(string $activity_classname, $model, $user) {
        $this->activity_classname = $activity_classname;
        $this->model = $model;
        $this->user = $user;
    }

    public static function getElementCustomActionsPermissions($element)
    {
        $flat_actions = [];
        $actions = static::actions();
        array_walk_recursive($actions, function ($a) use (&$flat_actions) {
            $flat_actions[] = $a;
        });

        $permissions = [];
        foreach ($flat_actions as $action_classname) {
            if (!$action_classname::standalone()) {
                $permissions[$action_classname] = $action_classname::authorize($element);
            }
        }

        return ['permissions' => $permissions];
    }

    public function jsonSerialize()
    {
        return array_merge_recursive(
            $this->model,
            ['comin_meta' => array_merge_recursive(
                $this->getElementCustomActionsPermissions($this->model)
            )]
        );
    }
}
