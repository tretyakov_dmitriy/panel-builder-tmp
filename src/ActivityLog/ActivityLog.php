<?php


namespace Digitech\PanelBuilder\ActivityLog;

use Digitech\PanelBuilder\StandaloneEntity;
use Digitech\PanelBuilder\Traits\HasActions;
use Digitech\PanelBuilder\Traits\HasFilters;

abstract class ActivityLog extends StandaloneEntity
{
    use HasActions, HasFilters;

    protected static $url;

    public static function type()
    {
        return 'activity_log';
    }

    abstract public static function name();

    public static function serialize($params = [])
    {
        return array_merge(
            parent::serialize($params),
            [
                'url' => static::$url,
                'name' => static::name(),
                'filters' => static::serializeFilters(),
                'actions' => static::serializeActions(static::actions()),
            ]);
    }
}
