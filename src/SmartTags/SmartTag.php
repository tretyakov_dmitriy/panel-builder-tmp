<?php

namespace Digitech\PanelBuilder\SmartTags;
use Digitech\PanelBuilder\Actions\RequestAction;
use Digitech\PanelBuilder\StandaloneEntity;

abstract class SmartTag extends StandaloneEntity
{

    public static function type()
    {
        return 'smart_tag';
    }

    abstract protected static function subject($params);

    protected static function fields() {
        return [];
    }

    protected static function data($subject) {
        return [];
    }

    protected static function labels()
    {
        return [];
    }

    /**
     * @return RequestAction[]
     */
    static function actions()
    {
        return [];
    }

    public static function serialize($params = [])
    {
        $subject = static::subject($params);

        $fields = [];
        foreach (static::fields() as $field) {
            $fields[] = $field->serialize();
        }

        $config = [
            'cache' => false,
            'subject' => $subject->asResourceElement()->merge(static::data($subject)),
            'labels' => static::labels(),
            'fields' => $fields,
            'actions' => static::serializeActions(static::actions(), $subject),
        ];
        return array_merge(parent::serialize($params), $config);
    }

    private static function serializeActions($actions, $subject)
    {
        $result = [];
        foreach($actions as $action_classname) {
            if($action_classname::authorize($subject)) {
                $result[] = $action_classname::serialize();
            }
        }
        return $result;
    }
}
