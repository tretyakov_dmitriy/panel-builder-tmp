<?php

namespace Digitech\PanelBuilder\Flexible;

class Layout
{
    private $name;

    public $fields = [], $id;

    public static function make(string $id, string $name, $fields)
    {
        $static = new static();

        $static->id = $id;
        $static->name = $name;
        $static->fields = $fields;

        return $static;
    }

    public function serialize() {
        $fields = [];
        foreach ($this->fields as $field) {
            if(!is_string($field)) {
                $fields[] = $field->serialize();
            } else {
                $fields[] = $field;
            }
        }
        return [
            'id' => $this->id,
            'name' => $this->name,
            'fields' => $fields,
        ];
    }
}
