<?php

namespace Digitech\PanelBuilder\Flexible;

abstract class Preset
{
    abstract public function layouts();
}
