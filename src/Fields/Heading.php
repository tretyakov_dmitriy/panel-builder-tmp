<?php

namespace Digitech\PanelBuilder\Fields;

class Heading
{
    private $text;
    public static function make($text)
    {
        $model = new static();
        $model->text = $text;
        return $model;
    }

    function serialize() {
        return [
            'control' => 'heading',
            'params' => [
                'text' => $this->text
            ]
        ];
    }
}
