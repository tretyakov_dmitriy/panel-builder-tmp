<?php


namespace Digitech\PanelBuilder\Fields;


class Textarea extends Field
{
    public static function make(string $attribute, string $name)
    {
        return parent::generate($attribute, $name, 'string', 'textarea', 'textarea', []);
    }

    public function pattern($value, $hint = null)
    {
        $this->params['pattern'] = $value;
        $this->params['pattern_hint'] = $hint;
        return $this;
    }
}
