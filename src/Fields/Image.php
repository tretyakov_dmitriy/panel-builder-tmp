<?php


namespace Digitech\PanelBuilder\Fields;


class Image extends Field
{

    public static function make(string $attribute, string $name)
    {
        return parent::generate($attribute, $name, 'file', 'image', 'image', ['settings' => [], 'full_width' => false]);
    }

    function asControl(string $uploadUrl)
    {
        $this->params['upload_url'] = $uploadUrl;
        return $this;
    }

    function fullWidth($value = true) {
        $this->params['full_width'] = $value;
        return $this;
    }

    function multiple() {
        $this->params['multiple'] = true;
        return $this;
    }

    public function settings($settings) {
        //        $settings = [
//            'resize' => [ // До какого размера уменьшить изображение после подтверждения кропа
//                'width' => 300,
//                'height' => 300
//            ],
//            'aspect_ratio' => 1, // Соотношение сторон
//            'min' => [ // Минимальный размер изображения после кропа
//                'width' => 300,
//                'height' => 300
//            ],
//            'round' => false, // Использовать круглый кроп
//        ];
        $this->params['settings'] = $settings;
        return $this;
    }
}
