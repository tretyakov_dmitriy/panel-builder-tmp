<?php

namespace Digitech\PanelBuilder\Fields;

class Percent extends Number
{
    public function serialize()
    {
        return array_merge(
            parent::serialize(),
            [
                'label' => 'percent'
            ]
        );
    }
}
