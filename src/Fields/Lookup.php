<?php


namespace Digitech\PanelBuilder\Fields;


class Lookup extends Field
{
    public $labelCallback, $controlResource;
    public static function make(string $attribute, string $name)
    {
        return parent::generate($attribute, $name, 'number', 'lookup', 'resource_element', ['chips' => true, 'search' => true]);
    }

    /**
     * @param \Closure $callback
     * @return $this
     */
    public function asLabel($callback)
    {
        $this->labelCallback = $callback;
        return $this;
    }

    public function asControl(string $target, array $query_params = [])
    {
        if(class_exists($target)) {
            $path = $target::$path . '/lookup';
            $this->controlResource = $target;
        } else {
            $path = $target;
        }

        if(count($query_params) > 0) {
            $path .= '?' . http_build_query($query_params);
        }
        $this->params['path'] = $path;
        return $this;
    }

    public function withoutChips() {
        $this->params['chips'] = false;
        return $this;
    }

    public function withoutSearch() {
        $this->params['search'] = false;
        return $this;
    }

    function multiple() {
        $this->params['multiple'] = true;
        if(!isset($this->params['default'])) {
            $this->params['default'] = [];
        }
        return $this;
    }

    public function serialize()
    {
        if($this->controlResource) {
            $this->params['add_resource_slug'] = $this->controlResource::create() ? $this->controlResource::slug() : null;
        }
        return parent::serialize();
    }
}
