<?php


namespace Digitech\PanelBuilder\Fields;


class Hidden extends Field
{
    public static function make(string $attribute)
    {
        return parent::generate($attribute, $name = '', 'any', null, null, []);
    }

    function default($value) {
        $this->params['default'] = $value;
        return $this;
    }
}
