<?php


namespace Digitech\PanelBuilder\Fields;


use Digitech\PanelBuilder\Builder;
use Digitech\PanelBuilder\Traits\HasDependencies;

abstract class Field
{
    protected $attribute, $name, $data_structure, $control, $label, $params = ['multiple' => false, 'locales' => []], $help = null;

    use HasDependencies;

    protected static function generate(string $attribute,string $name, $data_structure, $control, $label, array $params = []) {
        $result = new static();
        $result->attribute = $attribute;
        $result->name = $name;
        $result->data_structure = $data_structure;
        $result->control = $control;
        $result->label = $label;
        $result->params = array_merge($result->params, $params);
        return $result;
    }

    function getParameters() {
        return $this->params;
    }

    function getAttribute() {
        return $this->attribute;
    }

    function help($text) {
        $this->help = $text;
        return $this;
    }

    function name($name) {
        $this->name = $name;
        return $this;
    }

    public function translatable(bool $value = true) {
        $this->params['locales'] = Builder::$locales;
        return $this;
    }

    public function serialize() {
        return [
            'data_structure' => $this->data_structure,
            'attribute' => $this->attribute,
            'name' => $this->name,
            'control' => $this->control,
            'params' => $this->params,
            'dependencies' => $this->dependencies,
            'label' => $this->label,
            'help' => $this->help
        ];
    }
}
