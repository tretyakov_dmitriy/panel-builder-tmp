<?php


namespace Digitech\PanelBuilder\Fields;


class Select extends Field
{
    public static function make(string $attribute, string $name, array $options = [])
    {
        return parent::generate($attribute, $name, 'string', 'select', 'switch', ['options' => $options]);
    }

    public function default($value)
    {
        $this->params['default'] = $value;
        return $this;
    }

    public function multiple() {
        $this->params['multiple'] = true;
        return $this;
    }

    public function options(array $options) {
        $this->params['options'] = $options;
        return $this;
    }

    public function serialize()
    {
        $result = parent::serialize();
        $options = [];
        foreach($this->params['options'] as $value => $label) {
            $options[] = [
                'value' => $value,
                'label' => $label
            ];
        }
        $result['params']['options'] = $options;
        return $result;
    }
}
