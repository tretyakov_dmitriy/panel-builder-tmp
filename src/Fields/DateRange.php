<?php

namespace Digitech\PanelBuilder\Fields;

class DateRange extends Field
{
    public static function make(string $attribute,string $name)
    {
        return parent::generate($attribute, $name, 'date_range', 'date_range', 'date_range', []);
    }
}
