<?php


namespace Digitech\PanelBuilder\Fields;


class Text extends Field
{
    public static function make(string $attribute, string $name)
    {
        return parent::generate($attribute, $name, 'string', 'textbox', 'text', []);
    }

    public function default($value)
    {
        $this->params['default'] = $value;
        return $this;
    }

    public function pattern($value, $hint = null)
    {
        $this->params['pattern'] = $value;
        $this->params['pattern_hint'] = $hint;
        return $this;
    }

    function multiple() {
        $this->params['multiple'] = true;
        if(!isset($this->params['default'])) {
            $this->params['default'] = [null];
        }
        return $this;
    }
}
