<?php


namespace Digitech\PanelBuilder\Fields;


class Checkbox extends Field
{
    public static function make(string $attribute, string $name)
    {
        return parent::generate($attribute, $name, 'boolean', 'checkbox', 'boolean', [
            'default' => false // Чтобы при создании значение по умолчанию не было null
        ]);
    }

    function default($value) {
        $this->params['default'] = $value;
        return $this;
    }
}
