<?php


namespace Digitech\PanelBuilder\Fields;


use Digitech\PanelBuilder\Traits\HasDependencies;

class Container
{
    use HasDependencies;

    public $fields = [];

    private $id;

    private function __construct()
    {
        $this->id = md5(rand());
    }

    public static function make($fields)
    {
        $result = new static();
        $result->fields = $fields;
        return $result;
    }

    public function serialize()
    {
        $result = [];
        foreach ($this->fields as $field) {
            if(!is_string($field)) {
                $result[] = $field->serialize();
            } else {
                $result[] = $field;
            }
        }
        return [
            'control' => 'container',
            'id' => $this->id,
            'params' => [
                'fields' => $result
            ],
            'dependencies' => $this->dependencies,
        ];
    }
}
