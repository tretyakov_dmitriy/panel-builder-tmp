<?php

namespace Digitech\PanelBuilder\Fields;

class Currency extends Number
{
    public function currency($code) {
        $this->params['currency'] = $code;
        return $this;
    }

    public function currencyAttribute($attr) {
        $this->params['currency_attribute'] = $attr;
        return $this;
    }

    public function previous($attribute) {
        $this->params['previous'] = $attribute;
        return $this;
    }

    public function measure($attribute) {
        $this->params['measure'] = $attribute;
        return $this;
    }

    public function serialize()
    {
        return array_merge(
            parent::serialize(),
            [
                'label' => 'currency'
            ]
        );
    }
}
