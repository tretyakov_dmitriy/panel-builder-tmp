<?php

namespace Digitech\PanelBuilder\Fields;

class Audio extends File
{
    public function serialize()
    {
        return array_merge(
            parent::serialize(),
            [
                'label' => 'audio'
            ]
        );
    }
}
