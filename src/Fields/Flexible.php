<?php

namespace Digitech\PanelBuilder\Fields;

use Digitech\PanelBuilder\Flexible\Layout;
use Digitech\PanelBuilder\Flexible\Preset;

class Flexible extends Field
{
    public $layouts = [];
    public static function make(string $attribute, string $name)
    {
        return parent::generate($attribute, $name, 'array', 'flexible', 'flexible', ['multiple' => true]);
    }

    function addLayout(Layout $layout) {
        $this->layouts[] = $layout;
        return $this;
    }

    function preset($preset) {
        $this->layouts = (new $preset)->layouts();
        return $this;
    }

    public function serialize()
    {
        $result = parent::serialize();
        foreach ($this->layouts as $layout) {
            $result['params']['layouts'][] = $layout->serialize();
        }
        return $result;
    }
}
