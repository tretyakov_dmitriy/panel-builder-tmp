<?php


namespace Digitech\PanelBuilder\Fields;


class Number extends Field
{
    // https://angular.io/api/common/DecimalPipe
    public static function make(string $attribute, string $name)
    {
        return parent::generate($attribute, $name, 'number', 'textbox', 'number', [
            'format' => '1.0-0'
        ]);
    }

    function asLabel($format = '1.0-0')
    {
        $this->params['format'] = $format;
        return $this;
    }

    public function default($value)
    {
        $this->params['default'] = $value;
        return $this;
    }

    function multiple() {
        $this->params['multiple'] = true;
        if(!isset($this->params['default'])) {
            $this->params['default'] = [null];
        }
        return $this;
    }

    public function step(float $value)
    {
        $this->params['step'] = $value;
        return $this;
    }

    public function min(float $value)
    {
        $this->params['min'] = $value;
        return $this;
    }

    public function max(float $value)
    {
        $this->params['max'] = $value;
        return $this;
    }
}
