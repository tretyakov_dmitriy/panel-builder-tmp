<?php

namespace Digitech\PanelBuilder\Fields;

class Timer extends Number
{
    public function serialize()
    {
        return array_merge(
            parent::serialize(),
            [
                'label' => 'timer'
            ]
        );
    }
}
