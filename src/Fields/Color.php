<?php

namespace Digitech\PanelBuilder\Fields;

class Color extends Field
{
    public static function make(string $attribute, string $name)
    {
        return parent::generate($attribute, $name, 'string', 'colorpicker', 'color', []);
    }
}
