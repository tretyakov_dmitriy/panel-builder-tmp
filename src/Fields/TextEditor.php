<?php


namespace Digitech\PanelBuilder\Fields;


class TextEditor extends Field
{
    public static function make(string $attribute, string $name)
    {
        return parent::generate($attribute, $name, 'any', 'text-editor', 'html', []);
    }
}
