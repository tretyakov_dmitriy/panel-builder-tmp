<?php


namespace Digitech\PanelBuilder\Fields;


use Carbon\Carbon;

class DateTime extends Field
{
    public static function make(string $attribute,string $name)
    {
        return parent::generate($attribute, $name, 'number', 'datetimepicker', 'datetime');
    }

    public function default($value) {
        if ($value instanceof Carbon){
            $value = $value->getTimestamp();
        }
        if (!is_int($value)){
            return null;
        }
        $this->params['default'] = $value;
        return $this;
    }
}
