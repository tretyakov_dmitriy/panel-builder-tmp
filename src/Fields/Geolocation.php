<?php

namespace Digitech\PanelBuilder\Fields;

class Geolocation extends Field
{
    public static function make(string $attribute, string $name)
    {
        return parent::generate($attribute, $name, 'geolocation', 'geolocation', 'geolocation', []);
    }

    public function default($latitude, $longitude)
    {
        $this->params['default'] = [
            'latitude' => $latitude,
            'longitude' => $longitude,
        ];
        return $this;
    }
}
