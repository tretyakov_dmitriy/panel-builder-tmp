<?php


namespace Digitech\PanelBuilder\Fields;


class Group
{
    private $defaultLength = 0;
    public $attribute, $fields, $multiple = false;
    public static function make($attribute, $fields)
    {
        $result = new static();
        $result->fields = $fields;
        $result->attribute = $attribute;
        return $result;
    }

    public function serialize()
    {
        $result = [];
        foreach ($this->fields as $field) {
            if(!is_string($field)) {
                $result[] = $field->serialize();
            } else {
                $result[] = $field;
            }
        }
        return [
            'control' => 'group',
            'attribute' => $this->attribute,
            'params' => [
                'fields' => $result,
                'default_length' => $this->defaultLength,
                'multiple' => $this->multiple,
            ]
        ];
    }

    public function multiple($defaultLength = 1) {
        $this->multiple = true;
        $this->defaultLength = $defaultLength;

        return $this;
    }
}
