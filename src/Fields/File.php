<?php


namespace Digitech\PanelBuilder\Fields;


class File extends Field
{
    public static function make(string $attribute,string $name)
    {
        return parent::generate($attribute, $name, 'file', 'file', 'file');
    }

    function asLabel(string $linkUrl)
    {
        $this->params['link_url'] = $linkUrl;
        return $this;
    }

    function asControl(string $uploadUrl)
    {
        $this->params['upload_url'] = $uploadUrl;
        return $this;
    }

    function multiple() {
        $this->params['multiple'] = true;
        return $this;
    }
}
