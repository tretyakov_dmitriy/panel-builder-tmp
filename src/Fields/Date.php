<?php


namespace Digitech\PanelBuilder\Fields;


use Carbon\Carbon;

class Date extends Field
{
    public static function make(string $attribute,string $name)
    {
        return parent::generate($attribute, $name, 'number', 'datepicker', 'date', []);
    }

    public function default($value) {
        if ($value instanceof Carbon){
            $value = $value->getTimestamp();
        }
        if (!is_int($value)){
            return null;
        }
        $this->params['default'] = $value;
        return $this;
    }
}
