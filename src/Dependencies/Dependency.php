<?php

namespace Digitech\PanelBuilder\Dependencies;

class Dependency
{
    static function equal($attribute, $value) {
        return [
            'attribute' => $attribute,
            'type' => 'equal',
            'value' => $value
        ];
    }

    static function empty($attribute) {
        return [
            'attribute' => $attribute,
            'type' => 'empty'
        ];
    }

    static function notEmpty($attribute) {
        return [
            'attribute' => $attribute,
            'type' => 'not-empty'
        ];
    }

    static function in($attribute, $value) {
        return [
            'attribute' => $attribute,
            'type' => 'in',
            'value' => $value
        ];
    }
}
