<?php

namespace Digitech\PanelBuilder;

abstract class StandaloneEntity
{
    protected static $slug = null;

    abstract static function type();

    static function slug() {
        $slug = static::$slug;
        if (!$slug) {
            $slug = md5(static::class);
        }
        if(substr($slug, 0,1) !== '/') {
            $slug = '/' . $slug;
        }
        return $slug;
    }

    public static function serialize($params = []) {
        return [
            'id' => static::class,
            'type' => static::type(),
            'slug' => static::slug()
        ];
    }
}
