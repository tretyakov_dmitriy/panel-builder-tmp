<?php


namespace Digitech\PanelBuilder;

use Digitech\PanelBuilder\Auth\AuthProvider;
use Digitech\PanelBuilder\Menu\Folder;
use Digitech\PanelBuilder\Menu\Link;
use Digitech\PanelBuilder\Menu\Section;
use Digitech\PanelBuilder\Resources\ResourceElementLink;

class Builder
{
    static private $userResource;

    static private $basket;

    static public $locales = [];

    static private $registerMenuFunctions = [];

    static private $menuConfig = [];

    static private $dictionary = [
        'auth_providers' => [],
        'find' => []
    ];

    static function dictionary($classes) {
        foreach($classes as $class) {
            static::$dictionary['find'][$class] = $class;
            if(method_exists($class, 'slug')) {
                static::$dictionary['find'][$class::slug()] = $class;
            }
        }
    }

    static function setBasket($classname)
    {
        static::$basket = $classname;
    }

    /**
     * @deprecated
     */
    static function setResourcesDictionary(array $data)
    {
        static::dictionary($data);
    }

    /**
     * @deprecated
     */
    static function setReportsDictionary(array $data)
    {
        static::dictionary($data);
    }

    /**
     * @deprecated
     */
    static function setDashboardsDictionary(array $data)
    {
        static::dictionary($data);
    }

    /**
     * @deprecated
     */
    static function setSettingsDictionary(array $data)
    {
        static::dictionary($data);
    }

    /**
     * @deprecated
     */
    static function setWidgetsDictionary(array $data)
    {
        static::dictionary($data);
    }

    static function setSmartTagsDictionary(array $data)
    {
        static::dictionary($data);
    }

    static function addAuthProvider(AuthProvider $provider)
    {
        static::$dictionary['auth_providers'][] = $provider;
    }

    static function onMenuInit($func, int $sort = 1000)
    {
        static::$registerMenuFunctions[$sort][] = $func;
    }


    /**
     * @param $id
     * @param string $name
     * @param int|null $started_at
     * @param int|null $ended_at
     * @param null $avatar
     */

    public static function &findMenuSection($name)
    {
        foreach (static::$menuConfig as &$row) {
            if (get_class($row['element']) === Section::class && $row['element']->getName() === $name) {
                return $row['element'];
            }
        }
    }

    public static function &findMenuFolder($name)
    {
        foreach (static::$menuConfig as &$row) {
            if (get_class($row['element']) === Folder::class && $row['element']->getName() === $name) {
                return $row['element'];
            }
        }
    }


    static function addMenuSection(Section $section, $sort = null)
    {
        static::$menuConfig[] = [
            'element' => $section,
            'sort' => $sort === null ? 500 : $sort,
            'index' => count(static::$menuConfig)
        ];
    }

    static function addMenuFolder(Folder $folder, $sort = null)
    {
        static::$menuConfig[] = [
            'element' => $folder,
            'sort' => $sort === null ? 500 : $sort,
            'index' => count(static::$menuConfig)
        ];
    }

    static function addMenuLink(string $classname, string $icon, int $sort = 500)
    {
        $target = new $classname;

        $link = new Link($target::name([]), Link::generateLinkParams($classname), $icon);

        static::$menuConfig[] = [
            'element' => $link,
            'sort' => $sort,
            'index' => count(static::$menuConfig)
        ];

        return $link;
    }

    static function generateConfigParts($userResource, array $parts)
    {
        static::$userResource = $userResource;
        $response = [];
        if (isset($parts['find'])) {
            foreach ($parts['find'] as $entity) {
                if(is_string($entity)) {
                    $entity_id = $entity;
                    $entity_params = [];
                } else {
                    $entity_id = $entity['id'];
                    $entity_params = $entity['params'] ?? [];
                }
                if (isset(static::$dictionary['find'][$entity_id])) {
                    $entity = static::$dictionary['find'][$entity_id];
                    if(is_subclass_of($entity, StandaloneEntity::class)) {
                        $response['find'][$entity_id] = $entity::serialize($entity_params);
                    } else {
                        $response['find'][$entity_id] = (new $entity)->payload(); // FIXME Временное решение для виджетов, не учитывает возможные параметры в конструкторе
                    }
                } else {
                    throw new \Exception('Unknown entity id ' . $entity_id);
                }
            }
        }

        if (isset($parts['basket'])) {
            $response['basket'] = static::$basket ? static::$basket::serialize() : null;
        }

        if (isset($parts['auth_providers'])) {
            $response['auth_providers'] = static::$dictionary['auth_providers'];
        }

        if (isset($parts['menu'])) {
            $response['menu'] = Builder::generateMenuConfig();
        }

        if (isset($parts['user'])) {
            $response['user'] = new ResourceElementLink(static::$userResource, static::$userResource::user());
            $response['active_replacement'] = static::$userResource::activeReplacement();
            if ($response['active_replacement']) {
                $response['active_replacement'] = $response['active_replacement']->serialize();
            }
        }

        if (isset($parts['replacements'])) {
            $response = array_merge($response, Builder::generateReplacementsConfig());
        }

        if (isset($parts['scopes'])) {
            $response = array_merge($response, Builder::generateScopesConfig());
        }

        return $response;
    }

    static function generateReplacementsConfig()
    {
        $replacements = [];
        foreach (static::$userResource::availableReplacements() as $replacement) {
            $replacements[] = $replacement->serialize();
        }
        return [
            'replacements' => $replacements
        ];
    }

    static function generateScopesConfig()
    {
        $scopes = [];
        foreach (static::$userResource::availableScopes() as $scope) {
            $scopes[] = $scope->serialize();
        }
        return [
            'scopes' => $scopes
        ];
    }


    static function generateMenuConfig()
    {
        ksort(static::$registerMenuFunctions);
        foreach (static::$registerMenuFunctions as $sort => $closures) {
            foreach ($closures as $closure) {
                $closure();
            }

        }
        $menu = [];
        usort(static::$menuConfig, function ($item1, $item2) {
            $result = $item1['sort'] <=> $item2['sort'];
            if ($result === 0) {
                $result = $item1['element']->getName() <=> $item2['element']->getName();
            }
            return $result;
        });

        foreach (static::$menuConfig as $el) {
            $menu[] = $el['element']->serialize();
        }
        return $menu;
    }
}
