<?php


namespace Digitech\PanelBuilder\Replacements;

use Digitech\PanelBuilder\Resources\ResourceElement;
use Digitech\PanelBuilder\Resources\ResourceElementLink;

class Replacement
{
    private $replacedUser, $startedAt, $endedAt;
    private function __construct() {}

    static function make(ResourceElementLink $replacedUser, $started_at, $ended_at) {
        $model = new static();
        $model->replacedUser = $replacedUser;
        $model->startedAt = $started_at;
        $model->endedAt = $ended_at;

        return $model;
    }

    function serialize() {
        return [
            'user' => $this->replacedUser,
            'started_at' => $this->startedAt,
            'ended_at' => $this->endedAt
        ];
    }
}
