<?php


namespace Digitech\PanelBuilder\Reports;


use Digitech\PanelBuilder\Builder;
use Digitech\PanelBuilder\Fields\Lookup;
use Digitech\PanelBuilder\Fields\TreeLookup;
use Digitech\PanelBuilder\StandaloneEntity;
use Digitech\PanelBuilder\Traits\HasFilters;

abstract class Report extends StandaloneEntity
{
    use HasFilters;
    protected static $path;

    public static function type()
    {
        return 'report';
    }

    abstract static function view();

    protected static function description() {
        return null;
    }

    protected static function groupBy() {
        return null;
    }

    abstract public static function name();

    static function authorize()
    {
        return true;
    }

    protected static function fields() {
        return [];
    }

    public static function getElementRelationshipsData($element, $data) {
        $related_elements_response = [];
        $related_elements_resources_response = [];
        $fields = static::fields();
        $view = static::view();
        if ($view) {
            $fields = array_merge($fields, $view->columns);
        }
        foreach ($fields as $index => $field) {
            if (is_string($field)) {
                continue;
            }
            if (in_array(get_class($field), [Lookup::class, TreeLookup::class])) {
                if (!$field->labelCallback) {
                    continue;
                }
                if(!isset($data[$field->getAttribute()]) || (is_array($data[$field->getAttribute()]) && count($data[$field->getAttribute()]) === 0) || (!is_array($data[$field->getAttribute()]) && !$data[$field->getAttribute()])) {
                    /**
                     * Проверяем по data, потому что в мета информации нужно указывать не все связи, которые есть в модели, а только те, что отдаются клиенту
                     * Делается для уменьшения нагрузки
                     */
                    continue;
                }
                switch ($field->getParameters()['multiple']) {
                    case false:
                        $related_element = ($field->labelCallback)($element);
                        if ($related_element) {
                            $related_resource_classname = $related_element->getResourceName();
                            $related_elements_response[$related_resource_classname][(string)$related_resource_classname::getKey($related_element->model())] = SharedResource::prepareRelatedResourceElementData($related_element);
                            $related_elements_resources_response[$field->getAttribute()] = $related_resource_classname;
                        }
                        break;
                    case true:
                        $related_elements = ($field->labelCallback)($element);
                        foreach ($related_elements as $related_element) {
                            if($related_element) {
                                $related_resource_classname = $related_element->getResourceName();
                                $related_elements_response[$related_resource_classname][(string)$related_resource_classname::getKey($related_element->model())] = SharedResource::prepareRelatedResourceElementData($related_element);
                                $related_elements_resources_response[$field->getAttribute()][(string)$related_resource_classname::getKey($related_element->model())] = $related_resource_classname;
                            }
                        }
                        break;
                }
            }
        }
        return [
            'related_elements' => $related_elements_response,
            'related_elements_resources' => $related_elements_resources_response
        ];
    }

    static function serialize($params = [])
    {
        $fields = [];
        foreach (static::fields() as $field) {
            $fields[] = $field->serialize();
        }

        $description = static::description();

        $config = [
            'path' => static::$path,
            'name' => static::name(),
            'description' => $description,
            'fields' => $fields,
            'view' => static::view()->serialize(),
            'filters' => static::serializeFilters(),
            'group_by' => static::groupBy()
        ];
        return array_merge(parent::serialize($params), $config);
    }
}
