<?php

namespace Digitech\PanelBuilder\Reports;

class ReportRow implements \JsonSerializable
{
    private $model, $report;

    public function __construct(string $report, $model)
    {
        $this->report = $report;
        $this->model = $model;
    }

    public function jsonSerialize()
    {
        $result = array_merge_recursive(
            $this->model,
            ['comin_meta' => array_merge_recursive(
                $this->report::getElementRelationshipsData($this->model, $this->model)
            )
            ]
        );

        return $result;
    }
}
