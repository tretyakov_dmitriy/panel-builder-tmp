<?php

namespace Digitech\PanelBuilder\Basket;

class BasketItem implements \JsonSerializable
{
    private function __construct()
    {
    }

    private $type, $id, $name, $image = null, $quantity, $price, $count_min = 1, $count_step = 1, $count_max = null, $measure = null;
    public static function make($type, $id, $name, $quantity, $price) {
        $model = new static();
        $model->type  = $type;
        $model->id  = $id;
        $model->name  = $name;
        $model->quantity  = $quantity;
        $model->price  = $price;
        return $model;
    }

    function image($url) {
        $this->image = $url;
        return $this;
    }

    function countMin($value) {
        $this->count_min = $value;
        return $this;
    }

    function countMax($value) {
        $this->count_max = $value;
        return $this;
    }


    function countStep($value) {
        $this->count_step = $value;
        return $this;
    }

    function measure(string $value) {
        $this->measure = $value;
        return $this;
    }

    public function jsonSerialize()
    {
        return [
            'type' => $this->type,
            'id' => $this->id,
            'name' => $this->name,
            'image' => $this->image,
            'quantity' => $this->quantity,
            'price' => $this->price,
            'count_min' => $this->count_min,
            'count_max' => $this->count_max,
            'count_step' => $this->count_step,
            'measure' => $this->measure
        ];
    }
}
