<?php

namespace Digitech\PanelBuilder\Basket;


abstract class Basket implements \JsonSerializable
{
    abstract function path();

    abstract function checkoutAction();

    function properties() {
        return [];
    }

    function jsonSerialize()
    {
        $properties = [];
        foreach (static::properties() as $property) {
            $properties[] = $property->serialize();
        }
        return [
            'path' => $this->path(),
            'checkout_action' => $this->checkoutAction()::serialize(),
            'properties' => $properties,
        ];
    }

    function serialize(){
        return new static;
    }
}
