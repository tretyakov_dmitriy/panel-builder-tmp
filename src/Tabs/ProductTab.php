<?php


namespace Digitech\PanelBuilder\Tabs;


use Digitech\PanelBuilder\Fields\Container;
use Digitech\PanelBuilder\Fields\Field;
use Digitech\PanelBuilder\Traits\HasFields;

class ProductTab extends Tab
{
    use HasFields;

    private $image = null;

    public function __construct(string $name, array $fields = [])
    {
        parent::__construct('product', $name);
        $this->fields = $fields;
    }

    function image($field) {
        if(is_string($field)) {
            $this->image = $field;
        } else {
            $this->image = $field;
        }
        return $this;
    }

    public function serialize()
    {
        $fieldsData = [];
        foreach ($this->fields as $field) {
            if(!is_string($field)) {
                $fieldsData[] = $field->serialize();
            } else {
                $fieldsData[] = $field;
            }
        }
        return array_merge(parent::serialize(), [
            'product_fields' => [
                'image' => $this->image
            ],
            'other_fields' => $fieldsData,
        ]);
    }
}
