<?php


namespace Digitech\PanelBuilder\Tabs;


class ActivityLogTab extends Tab
{
    public function __construct(string $name, string $classname) {
        // classname пока что не учитывается, журнал единый на всю систему, но это может измениться
        parent::__construct('activity_log', $name);
        $this->data = [
            'activity_log_id' => $classname
        ];
    }

    public function serialize()
    {
        return array_merge(parent::serialize(), $this->data);
    }
}
