<?php


namespace Digitech\PanelBuilder\Tabs;

class HasManyTab extends Tab
{
    public function __construct($name, $resourceClassName, $parentAttribute)
    {
        parent::__construct('has_many', $name);
        $this->data = [
            'resource_id' => $resourceClassName,
            'scope' => str_contains($parentAttribute, '[]') ? [str_replace('[]', '', $parentAttribute) => [':id']] : [$parentAttribute => ':id'],
        ];
    }

    public function scope(array $scope) {
        $this->data['scope'] = array_merge($scope, $this->data['scope']);
        return $this;
    }

    public function serialize() {
        $parentData = parent::serialize();

        return array_merge(
            $parentData,
            $this->data
        );
    }
}
