<?php


namespace Digitech\PanelBuilder\Tabs;


class SurveyTab extends Tab
{
    public function __construct($name, $url, $attribute)
    {
        parent::__construct('survey', $name);
        $this->data = [
            'config' => [
                'url' => $url,
                'attribute' => $attribute
            ]
        ];
    }

    public function serialize()
    {
        return array_merge(parent::serialize(), $this->data);
    }
}
