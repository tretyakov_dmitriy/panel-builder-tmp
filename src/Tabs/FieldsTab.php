<?php


namespace Digitech\PanelBuilder\Tabs;


use Digitech\PanelBuilder\Fields\Container;
use Digitech\PanelBuilder\Fields\Field;
use Digitech\PanelBuilder\Traits\HasFields;

class FieldsTab extends Tab
{
    use HasFields;

    private $actions_links = [];

    public function __construct(string $name, array $fields = [])
    {
        parent::__construct('fields', $name);
        $this->fields = $fields;
    }

    function withActions($actions) {
        $this->actions_links = $actions;
        return $this;
    }

    public function serialize()
    {
        $fieldsData = [];
        foreach ($this->fields as $field) {
            if(!is_string($field)) {
                $fieldsData[] = $field->serialize();
            } else {
                $fieldsData[] = $field;
            }
        }
        return array_merge(parent::serialize(), [
            'fields' => $fieldsData,
            'actions_links' => $this->actions_links
        ]);
    }
}
