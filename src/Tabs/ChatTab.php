<?php


namespace Digitech\PanelBuilder\Tabs;


class ChatTab extends Tab
{
    public function __construct($name, $url, $upload_url = null, $link_url = null)
    {
        parent::__construct('chat', $name);
        $this->data = [
            'config' => [
                'url' => $url,
                'files' => [
                    'upload_url' => $upload_url,
                    'link_url' => $link_url
                ],
            ]
        ];
    }

    public function serialize()
    {
        return array_merge(parent::serialize(), $this->data);
    }
}
