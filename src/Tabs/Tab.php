<?php


namespace Digitech\PanelBuilder\Tabs;

use Digitech\PanelBuilder\Traits\HasDependencies;

abstract class Tab
{
    use HasDependencies;
    protected $name, $expanded = false;

    protected $type, $data;

    public function __construct(string $type, string $name)
    {
        $this->type = $type;
        $this->name = $name;
    }

    public function serialize()
    {
        return [
            'name' => $this->name,
            'expanded' => $this->expanded,
            'type' => $this->type,
            'dependencies' => $this->dependencies
        ];
    }

    public function expanded() {
        $this->expanded = true;
        return $this;
    }
}
