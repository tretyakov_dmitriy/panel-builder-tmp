<?php

namespace Digitech\PanelBuilder\Auth;

class OAuthProvider implements \JsonSerializable, AuthProvider
{
    private $logo, $code_url, $login_url;

    public function __construct(string $id, string $logo, string $code_url, string $login_url)
    {
        $this->id = $id;
        $this->logo = $logo;
        $this->code_url = $code_url;
        $this->login_url = $login_url;
    }

    function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'type' => 'oauth',
            'logo' => $this->logo,
            'code_url' => $this->code_url,
            'login_url' => $this->login_url
        ];
    }
}
