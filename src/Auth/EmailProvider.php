<?php

namespace Digitech\PanelBuilder\Auth;

class EmailProvider implements \JsonSerializable, AuthProvider
{
    private $url, $usernameParameterName, $passwordParameterName;

    public function __construct(string $url, $usernameParameterName = 'email', $passwordParametername = 'password')
    {
        $this->url = $url;
        $this->usernameParameterName = $usernameParameterName;
        $this->passwordParameterName = $passwordParametername;
    }

    function jsonSerialize(): mixed
    {
        return [
            'type' => 'email',
            'url' => $this->url,
            'email_parameter_name' => $this->usernameParameterName,
            'password_parameter_name' => $this->passwordParameterName
        ];
    }
}
