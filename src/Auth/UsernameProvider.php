<?php

namespace Digitech\PanelBuilder\Auth;

class UsernameProvider implements \JsonSerializable, AuthProvider
{
    private $url, $usernameParameterName, $passwordParameterName;

    public function __construct(string $url, $usernameParameterName = 'login', $passwordParametername = 'password')
    {
        $this->url = $url;
        $this->usernameParameterName = $usernameParameterName;
        $this->passwordParameterName = $passwordParametername;
    }

    function jsonSerialize(): mixed
    {
        return [
            'type' => 'username',
            'url' => $this->url,
            'username_parameter_name' => $this->usernameParameterName,
            'password_parameter_name' => $this->passwordParameterName
        ];
    }
}
