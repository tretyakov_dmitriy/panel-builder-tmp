<?php

namespace Digitech\PanelBuilder\Scopes;

class Scope
{
    private $id, $name, $headers = [];

    private function __construct() {}

    static function make($id, $name) {
        $model = new static();
        $model->id = $id;
        $model->name = $name;
        return $model;
    }

    function headers(array $headers) {
        $this->headers = $headers;
        return $this;
    }

    function serialize() {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'headers' => $this->headers
        ];
    }
}
