<?php


namespace Digitech\PanelBuilder\Menu;

use Digitech\PanelBuilder\ActivityLog\ActivityLog;
use Digitech\PanelBuilder\Dashboards\Dashboard;
use Digitech\PanelBuilder\Reports\Report;
use Digitech\PanelBuilder\Resources\Resource;
use Digitech\PanelBuilder\Settings\Settings;

class Link
{
    private $name, $icon, $params, $badge = null;

    public function __construct($name, $params, $icon = null)
    {
        $this->name = $name;
        $this->icon = $icon;
        $this->params = $params;
        return $this;
    }

    public function getId() {
        return 'link_' . md5($this->name);
    }

    public function getName() {
        return $this->name;
    }

    /**
     * @param int | callable $badge
     */
    function withBadge($badge) {
        $this->badge = $badge;
    }

    public function serialize() {
        if($this->badge instanceof \Closure) {
            $this->badge = ($this->badge)();
        }
        if(!$this->badge) {
            $this->badge = null; // На случай если 0
        }
        return [
            'name' => $this->name,
            'icon' => $this->icon,
            'type' => 'link',
            'badge' =>  $this->badge,
            'params' => $this->params,
        ];
    }

    public static function generateLinkParams($classname) {
        $params = [];
        if(is_subclass_of($classname, Resource::class)) {
            $params = ['target_type' => 'resource', 'target_slug' => $classname::slug()];
        }
        else if(is_subclass_of($classname, Settings::class)) {
            $params = ['target_type' => 'settings', 'target_slug' => $classname::slug()];
        }
        else if (is_subclass_of($classname, Report::class)) {
            $params = ['target_type' => 'report', 'target_slug' => $classname::slug()];
        }
        else if (is_subclass_of($classname, ActivityLog::class)) {
            $params = ['target_type' => 'activity_log'];
        }
        else if (is_subclass_of($classname, Dashboard::class)) {
            $params = ['target_type' => 'dashboard', 'target_slug' => $classname::slug()];
        }
        if(empty($params)) {
            $parent_classname = get_parent_class($classname);
            if($parent_classname) {
                return static::generateLinkParams($parent_classname);
            }
        }
        return $params;
    }
}
