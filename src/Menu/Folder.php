<?php


namespace Digitech\PanelBuilder\Menu;

use Digitech\PanelBuilder\ActivityLog\ActivityLog;
use Digitech\PanelBuilder\Reports\Report;
use Digitech\PanelBuilder\Resources\Resource;
use Digitech\PanelBuilder\Settings\Settings;

class Folder
{

    private $name, $icon, $links = [];

    public function __construct($name, $icon = null)
    {
        $this->name = $name;
        $this->icon = $icon;
        return $this;
    }

    /**
     * @param string $classname
     * @param int $sort
     */
    public function addLink(string $classname, int $sort = 500) {
        $params = Link::generateLinkParams($classname);
        if(empty($params)) {
            throw new \Exception('Unknown menu link type '. $classname);
        }
        $link = new Link($classname::name([]), $params); // Пустой массив чтобы Дашбоард не ругался на отсутствие параметров
        $this->links[] = [
            'element'=> $link,
            'sort' => $sort,
            'index' => count($this->links)
        ];

        return $link;
    }

    public function getId() {
        return 'folder_' . md5($this->name);
    }

    public function getName() {
        return $this->name;
    }

    public function serialize() {
        $folderId = $this->getId();
        $links = [];
        usort($this->links, function ($item1, $item2) {
            $result = $item1['sort'] <=> $item2['sort'];
            if ($result === 0) {
                $result = $item1['element']->getName() <=> $item2['element']->getName();
            }
            return $result;
        });
        $badge = 0;
        foreach($this->links as $link) {
            $linkConfig = $link['element']->serialize();
            $linkConfig['id'] = $folderId . '_' . $link['element']->getId();
            if($linkConfig['badge']) {
                if(is_numeric($linkConfig['badge'])) {
                    $badge += $linkConfig['badge'];
                } else {
                    $badge = true;
                }
            }
            $links[] = $linkConfig;
        }
        return [
            'id' => $folderId,
            'name' => $this->name,
            'icon' => $this->icon,
            'type' => 'folder',
            'badge' =>  $badge ? $badge : null,
            'links' => $links
        ];
    }
}
