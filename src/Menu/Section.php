<?php

namespace Digitech\PanelBuilder\Menu;

use Digitech\PanelBuilder\ActivityLog\ActivityLog;
use Digitech\PanelBuilder\Reports\Report;
use Digitech\PanelBuilder\Resources\Resource;
use Digitech\PanelBuilder\Settings\Settings;

class Section
{
    private $name, $description, $children = [];

    public function __construct($name, $description = null)
    {
        $this->name = $name;
        $this->description = $description;
        return $this;
    }

    public function addFolder(string $name, $icon, int $sort = 500) {
        $link = new Folder($name, $icon);
        $this->children[] = [
            'element'=> $link,
            'sort' => $sort,
            'index' => count($this->children)
        ];

        return $link;
    }

    public function &findFolder($name)
    {
        foreach ($this->children as &$row) {
            if (get_class($row['element']) === Folder::class && $row['element']->getName() === $name) {
                return $row['element'];
            }
        }
    }

    public function addLink(string $classname, string $icon, int $sort = 500) {
        $params = Link::generateLinkParams($classname);
        if(empty($params)) {
            throw new \Exception('Unknown menu link class '. $classname);
        }
        $link = new Link($classname::name(), $params, $icon);
        $this->children[] = [
            'element'=> $link,
            'sort' => $sort,
            'index' => count($this->children)
        ];

        return $link;
    }

    public function getId() {
        return 'section_' . md5($this->name);
    }

    public function getName() {
        return $this->name;
    }

    public function serialize() {
        $sectionId = $this->getId();
        $children = [];
        usort($this->children, function ($item1, $item2) {
            $result = $item1['sort'] <=> $item2['sort'];
            if ($result === 0) {
                $result = $item1['element']->getName() <=> $item2['element']->getName();
            }
            return $result;
        });
        foreach($this->children as $item) {
            $itemConfig = $item['element']->serialize();
            $itemConfig['id'] = $sectionId . '_' . $item['element']->getId();
            $children[] = $itemConfig;
        }
        return [
            'id' => $sectionId,
            'name' => $this->name,
            'description' => $this->description,
            'type' => 'section',
            'children' => $children
        ];
    }
}
