<?php

namespace Digitech\PanelBuilder\Commands;

class OpenResourceElement implements \JsonSerializable
{
    private $resource, $element_id;
    public function __construct(string $resource, $element_id)
    {
        $this->resource = $resource;
        $this->element_id = $element_id;
    }

    function jsonSerialize()
    {
        return [
            'type' => 'open-resource-element',
            'params' => [
                'resource_id' => $this->resource,
                'element_id' => $this->element_id
            ]
        ];
    }

}
