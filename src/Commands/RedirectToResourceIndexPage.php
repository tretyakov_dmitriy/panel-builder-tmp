<?php

namespace Digitech\PanelBuilder\Commands;

class RedirectToResourceIndexPage implements \JsonSerializable
{
    private $resource;
    public function __construct(string $resource)
    {
        $this->resource = $resource;
    }

    function jsonSerialize()
    {
        return [
            'type' => 'redirect-to-resource-index-page',
            'params' => [
                'resource_slug' => $this->resource::slug()
            ]
        ];
    }

}
