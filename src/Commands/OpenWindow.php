<?php

namespace Digitech\PanelBuilder\Commands;

class OpenWindow implements \JsonSerializable
{
    private $url;
    public function __construct(string $url)
    {
        $this->url = $url;
    }

    function jsonSerialize()
    {
        return [
            'type' => 'open-window',
            'params' => [
                'url' => $this->url
            ]
        ];
    }

}
