<?php

namespace Digitech\PanelBuilder\Commands;

class RefreshBasket implements \JsonSerializable
{
    function jsonSerialize()
    {
        return [
            'type' => 'refresh-basket'
        ];
    }
}
