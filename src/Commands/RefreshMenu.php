<?php

namespace Digitech\PanelBuilder\Commands;

class RefreshMenu implements \JsonSerializable
{
    function jsonSerialize()
    {
        return [
            'type' => 'refresh-menu'
        ];
    }
}
