<?php
namespace Digitech\PanelBuilder\Charts;

class LineChart extends Chart
{
        public function __construct()
        {
            parent::__construct('line');
        }
}
