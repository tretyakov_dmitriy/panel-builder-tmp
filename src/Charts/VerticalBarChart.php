<?php
namespace Digitech\PanelBuilder\Charts;

class VerticalBarChart extends Chart
{
        public function __construct()
        {
            parent::__construct('vertical-bar');
        }
}
