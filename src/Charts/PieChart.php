<?php
namespace Digitech\PanelBuilder\Charts;

class PieChart extends Chart
{
        public function __construct()
        {
            parent::__construct('pie');
        }
}
