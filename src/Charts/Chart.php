<?php


namespace Digitech\PanelBuilder\Charts;


abstract class Chart
{
    protected $type, $results;

    public function __construct($type)
    {
        $this->type = $type;
    }

    static function make()
    {
        $static = new static();
        return $static;
    }

    public function results($data)
    {
        $this->results = $data;
        return $this;
    }

    function serialize()
    {
        return [
            'type' => $this->type,
            'results' => $this->results
        ];
    }
}
