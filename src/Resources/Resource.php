<?php


namespace Digitech\PanelBuilder\Resources;

use Digitech\PanelBuilder\Builder;
use Digitech\PanelBuilder\Fields\Container;
use Digitech\PanelBuilder\Fields\Flexible;
use Digitech\PanelBuilder\Fields\Group;
use Digitech\PanelBuilder\Fields\Lookup;
use Digitech\PanelBuilder\Fields\TreeLookup;
use Digitech\PanelBuilder\StandaloneEntity;
use Digitech\PanelBuilder\Tabs\FieldsTab;
use Digitech\PanelBuilder\Traits\HasActions;
use Digitech\PanelBuilder\Traits\HasFilters;
use Digitech\PanelBuilder\Traits\PureResource;
use Digitech\PanelBuilder\Views\TabularView;

abstract class Resource extends StandaloneEntity
{
    use HasFilters, HasActions;

    /**
     * @return null | TabularView
     */
    public static function indexPage()
    {
        return null;
    }

    public static function type()
    {
        return 'resource';
    }

    public static function title($element)
    {
        return null;
    }

    public static function avatar($element)
    {
        return false;
    }

    public static function name()
    {
        return null;
    }

    public static function getKey($element) {
        return is_array($element) ? $element['id'] : $element->id;
    }

    public static function createElement($data) {
        if($data) {
            return new ResourceElement(static::class, $data);
        }
    }

    public static $path;
    protected static $search = true;

    protected static function unread($element)
    {
        return false;
    }

    /**
     * @return string[]
     */
    static function orderBy()
    {
        return [];
    }

    public static function createPage()
    {
        return null;
    }

    public static function editPage()
    {
        return null;
    }

    public static function viewPage()
    {
        return null;
    }

    protected static function fields()
    {
        return [];
    }

    public static function viewAny()
    {
        return false;
    }

    public static function view($model)
    {
        return false;
    }

    public static function create()
    {
        return false;
    }

    public static function update($model)
    {
        return false;
    }

    public static function delete($model)
    {
        return false;
    }

    /**
     * @return mixed
     */
    public static function getPath()
    {
        return static::$path;
    }

    public static function serialize($params = [])
    {
        $createPage = static::createPage();
        $editPage = static::editPage();
        $viewPage = static::viewPage();
        $fields = [];
        foreach (static::fields() as $field) {
            $fields[] = $field->serialize();
        }

        $indexPage = static::indexPage();

        $result = [
            'path' => static::$path,
            'name' => static::name(),
            'pure' => in_array(PureResource::class, class_uses_recursive(static::class)),
            'search' => static::$search,
            'fields' => $fields,
            'order_by' => static::orderBy(),
            'filters' => static::serializeFilters(),
            'actions' => static::serializeActions(static::actions()),
            'index' => $indexPage ? $indexPage->serialize() : null,
            'create' => $createPage ? $createPage->serialize() : null,
            'edit' => $editPage ? $editPage->serialize() : null,
            'view' => $viewPage ? $viewPage->serialize() : null,
            'permissions' => [
                'view_any' => static::viewAny(),
                'create' => static::create()
            ]
        ];

        return array_merge(parent::serialize($params), $result);
    }

    public static function getElementCustomActionsPermissions($element)
    {
        $flat_actions = [];
        $actions = static::actions();
        array_walk_recursive($actions, function ($a) use (&$flat_actions) {
            $flat_actions[] = $a;
        });

        $permissions = [];
        foreach ($flat_actions as $action_classname) {
            if (!$action_classname::standalone()) {
                $permissions[$action_classname] = $action_classname::authorize($element);
            }
        }

        return ['permissions' => $permissions];
    }

    public static function getElementDefaultActionsPermissions($element)
    {
        return [
            'update' => static::update($element),
            'view' => static::view($element),
            'delete' => static::delete($element),
        ];
    }

    public static function prepareRelatedResourceElementData($related_element)
    {
        $resource = $related_element->getResourceName();
        $arr = [
            'resource_slug' => $resource::slug(),
            'title' => $resource::title($related_element->model()),
            'avatar' => $resource::avatar($related_element->model()),
        ];
        if (!$resource::viewPage()) {
            $arr['permissions']['view'] = false;
        } else {
            $arr['permissions']['view'] = $resource::view($related_element->model());
        }
        return $arr;
    }

    public static function getElementAdminData($element)
    {
        return [
            'unread' => static::unread($element),
        ];
    }

    private static function filterFieldsFromResource($result = [])
    {
        $fields = static::fields();
        $viewPage = static::viewPage(); // Потому что в странице просмотра тоже можно инициализировать контролы
        if ($viewPage) {
            $fields = array_merge($fields, $viewPage->mainAttributes);
            foreach ($viewPage->tabs as $tab) {
                if ($tab instanceof FieldsTab) {
                    $fields = array_merge($fields, $tab->fields);
                }
            }
        }
        return $fields;
    }


    private static function generateRelationshipsDataFromFields($element, $data, $fields, &$related_elements_response = [], &$related_elements_resources_response = [])
    {
        foreach ($fields as $index => $field) {
            if (is_string($field)) {
                continue;
            }
            if (get_class($field) === Container::class) {
                static::generateRelationshipsDataFromFields($element, $data, $field->fields, $related_elements_response, $related_elements_resources_response);
            }
            if (get_class($field) === Group::class) {
                if(isset($data[$field->attribute])) {
                    if($field->multiple) {
                        foreach($data[$field->attribute] as $data_block) {
                            static::generateRelationshipsDataFromFields($element, $data_block, $field->fields, $related_elements_response, $related_elements_resources_response);
                        }
                    } else {
                        static::generateRelationshipsDataFromFields($element, $data[$field->attribute], $field->fields, $related_elements_response, $related_elements_resources_response);
                    }
                }
            }
            if(get_class($field) === Flexible::class && isset($data[$field->getAttribute()])) {
                foreach($data[$field->getAttribute()] as $block_data) {
                    foreach($field->layouts as $layout) {
                        if($layout->id === $block_data['layout']) {
                            static::generateRelationshipsDataFromFields($element, $block_data['attributes'], $layout->fields, $related_elements_response, $related_elements_resources_response);
                        }
                    }
                }
            }


            if (in_array(get_class($field), [Lookup::class, TreeLookup::class])) {
                if (!$field->labelCallback) {
                    continue;
                }
                if (!isset($data[$field->getAttribute()]) || (is_array($data[$field->getAttribute()]) && count($data[$field->getAttribute()]) === 0) || (!is_array($data[$field->getAttribute()]) && !$data[$field->getAttribute()])) {
                    /**
                     * Проверяем по data, потому что в мета информации нужно указывать не все связи, которые есть в модели, а только те, что отдаются клиенту
                     * Делается для уменьшения нагрузки
                     */
                    continue;
                }
                switch ($field->getParameters()['multiple']) {
                    case false:
                        $related_element = ($field->labelCallback)($element, $data);
                        if ($related_element) {
                            $related_resource_classname = $related_element->getResourceName();
                            $related_elements_response[$related_resource_classname][(string)$related_resource_classname::getKey($related_element->model())] = static::prepareRelatedResourceElementData($related_element);
                            $related_elements_resources_response[$field->getAttribute()] = $related_resource_classname;
                        }
                        break;
                    case true:
                        $related_elements = ($field->labelCallback)($element, $data);
                        foreach ($related_elements as $related_element) {
                            if ($related_element) {
                                $related_resource_classname = $related_element->getResourceName();
                                $related_elements_response[$related_resource_classname][(string)$related_resource_classname::getKey($related_element->model())] = static::prepareRelatedResourceElementData($related_element);
                                $related_elements_resources_response[$field->getAttribute()][(string)$related_resource_classname::getKey($related_element->model())] = $related_resource_classname;
                            }
                        }
                        break;
                }
            }
        }
    }

    public static function getElementRelationshipsData($element, $data, &$related_elements_response = [], &$related_elements_resources_response = [])
    {
        $fields = static::filterFieldsFromResource();

        self::generateRelationshipsDataFromFields($element, $data, $fields, $related_elements_response, $related_elements_resources_response);

        return [
            'related_elements' => $related_elements_response,
            'related_elements_resources' => $related_elements_resources_response
        ];
    }

    public static function getElementLinkData($element)
    {
        return [
            'title' => static::title($element),
            'avatar' => static::avatar($element),
            'element_id' => static::getKey($element),
            'resource_id' => static::class,
            'resource_slug' => static::slug(),
            'permissions' => static::getElementDefaultActionsPermissions($element)
        ];
    }
}
