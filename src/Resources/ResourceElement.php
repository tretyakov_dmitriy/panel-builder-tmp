<?php

namespace Digitech\PanelBuilder\Resources;

class ResourceElement implements \JsonSerializable
{
    private $model, $resource, $data = [], $usingCustomData = false, $folder = false, $without_custom_actions_permissions = false, $deletable = true;

    public function __construct(string $resource, $model)
    {
        $this->resource = $resource;
        $this->model = $model;
    }

    public function model() {
        return $this->model;
    }

    public function merge(array $data)
    {
        $this->data = array_merge($this->data, $data);
        return $this;
    }

    public function markAsFolder($deletable = true) {
        $this->folder = true;
        $this->deletable = $deletable;
        return $this;
    }

    public function withoutCustomActionsPermissions() {
        $this->without_custom_actions_permissions = true;
        return $this;
    }

    public function data(array $data)
    {
        $this->usingCustomData = true;
        $this->data = $data;
        return $this;
    }

    public function getResourceName()
    {
        return $this->resource;
    }

    private function fillModel($model, $attributes) {
        if(!is_object($model)) {
            foreach ($attributes as $key => $value) {
                $model[$key] = $value;
            }
        } else {
            if(method_exists($model, 'setAttribute')) { // FIXME костыль для Laravel
                foreach ($attributes as $key => $value) {
                    $model->setAttribute($key, $value);
                }
            } else {
                foreach ($attributes as $key => $value) {
                    $model->$key = $value;
                }
            }
        }

        return $model;
    }

    public function jsonSerialize()
    {
        if(!$this->usingCustomData) {
            $this->data = array_merge(is_array($this->model) ? $this->model : $this->model->jsonSerialize(), $this->data);
        }
        $filled_model = $this->fillModel($this->model, $this->data); // Потому что поля в data в приоритете
        $result = array_merge_recursive(
            $this->data,
            ['comin_meta' => array_merge_recursive(
                [
                    'folder' => $this->folder,
                ],
                $this->resource::getElementAdminData($filled_model),
                $this->without_custom_actions_permissions ? [] : $this->resource::getElementCustomActionsPermissions($filled_model),
                $this->resource::getElementRelationshipsData($filled_model, $this->data),
                $this->resource::getElementLinkData($filled_model)
            )]
        );

        if(!$this->deletable) {
            if($result['comin_meta']['permissions']['delete'] === true) {
                $result['comin_meta']['permissions']['delete'] = false;
            }
        }

        return $result;
    }
}
