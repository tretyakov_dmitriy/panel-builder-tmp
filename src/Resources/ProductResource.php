<?php

namespace Digitech\PanelBuilder\Resources;

class ProductResource extends Resource
{
    public static function createElement($data) {
        if($data) {
            return new ProductResourceElement(static::class, $data);
        }
    }
}
