<?php

namespace Digitech\PanelBuilder\Resources;

class ProductResourceElement extends ResourceElement
{
    private $variants = [];
    public function variants(array $variants) {
        $this->variants = $variants;
        return $this;
    }

    public function jsonSerialize() {
        $result = parent::jsonSerialize();
        $result['comin_meta']['variants'] = $this->variants;
        return $result;
    }

}
