<?php

namespace Digitech\PanelBuilder\Resources;

class ResourceElementLink implements \JsonSerializable
{
    private $model, $resource, $data = [];

    public function __construct(string $resource, $model)
    {
        $this->resource = $resource;
        $this->model = $model;
    }
    public function getResourceName() {
        return $this->resource;
    }

    public function jsonSerialize()
    {
        return $this->resource::getElementLinkData($this->model);
    }
}
