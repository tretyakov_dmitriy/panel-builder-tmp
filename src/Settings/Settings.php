<?php


namespace Digitech\PanelBuilder\Settings;

use Digitech\PanelBuilder\StandaloneEntity;

abstract class Settings extends StandaloneEntity
{

    static protected $id, $path;
    public static abstract function page();

    abstract public static function name();

    public static function type()
    {
        return 'settings';
    }

    public static function serialize($params = [])
    {
        $page = static::page();

        $additional = [];

        return array_merge(
            parent::serialize($params),
            [
                'name' => static::name(),
                'path' => static::$path,
                'page' => $page ? $page->serialize() : null,
            ],
            $additional
        );
    }
}
