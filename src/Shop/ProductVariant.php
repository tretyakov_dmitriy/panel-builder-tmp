<?php

namespace Digitech\PanelBuilder\Shop;

class ProductVariant implements \JsonSerializable
{
    private function __construct()
    {
    }

    private $type, $id, $name, $price, $images = [], $old_price = null, $measure = null, $count_min = 1, $count_initial = 1, $count_step = 1, $count_max = null;
    public static function make($type, $id, $name, $price) {
        $model = new static();
        $model->type  = $type;
        $model->id  = $id;
        $model->name  = $name;
        $model->price  = $price;
        return $model;
    }

    function oldPrice($value) {
        $this->old_price = $value;
        return $this;
    }

    function measure(string $measure) {
        $this->measure = $measure;
        return $this;
    }

    function images(array $value) {
        $this->images = $value;
        return $this;
    }

    function countMin($value) {
        $this->count_min = $value;
        return $this;
    }

    function countMax($value) {
        $this->count_max = $value;
        return $this;
    }


    function countInitial($value) {
        $this->count_initial = $value;
        return $this;
    }

    function countStep($value) {
        $this->count_step = $value;
        return $this;
    }

    public function jsonSerialize()
    {
        return [
            'type' => $this->type,
            'id' => $this->id,
            'name' => $this->name,
            'price' => $this->price,
            'old_price' => $this->old_price,
            'measure' => $this->measure,
            'images' => $this->images,
            'count_min' => $this->count_min,
            'count_max' => $this->count_max,
            'count_step' => $this->count_step,
            'count_initial' => $this->count_initial,
        ];
    }
}
