<?php


namespace Digitech\PanelBuilder\Pages;

use Digitech\PanelBuilder\Tabs\ActivityLogTab;
use Digitech\PanelBuilder\Tabs\ChatTab;
use Digitech\PanelBuilder\Tabs\HasManyTab;
use Digitech\PanelBuilder\Traits\HasWebHooks;

/**
 * Class ViewPage
 * @package Digitech\PanelBuilder\Pages
 * @property ActivityLogTab[] | ChatTab[] | HasManyTab[] $tabs
 */
class ViewPage extends DetailPage
{
    use HasWebHooks;

    public function __construct()
    {
        parent::__construct('view');
    }

    /**
     * @var string[] $mainAttributes
     */
    public $mainAttributes = [];

    public function serialize()
    {
        $result = parent::serialize();
        $result['main_attributes'] = $this->mainAttributes;
        $result['web_hooks'] = [];

        foreach($this->webHooks as $webHook) {
            $result['web_hooks'][] = $webHook->serialize();
        }

        return $result;
    }
}
