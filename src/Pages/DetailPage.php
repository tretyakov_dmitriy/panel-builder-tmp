<?php


namespace Digitech\PanelBuilder\Pages;


abstract class DetailPage
{
    public $tabs = [];
    protected $key = '';
    private $verificationUrl = null;

    function __construct($key)
    {
    }

    function verificationUrl($url) {
        $this->verificationUrl = $url;
    }

    function serialize() {
        $tabsConfig = [];
        foreach ($this->tabs as $tabConfig) {
            $tabsConfig[] = $tabConfig->serialize();
        }
        return [
            'tabs' => $tabsConfig,
            'verification_url' => $this->verificationUrl
        ];
    }
}
