<?php


namespace Digitech\PanelBuilder\Pages;

use Digitech\PanelBuilder\Tabs\FieldsTab;

/**
 * Class EditPage
 * @package Digitech\PanelBuilder\Pages
 * @property FieldsTab[] $tabs
 */
class EditPage extends DetailPage
{
    public function __construct()
    {
        parent::__construct('edit');
    }

    public function serialize()
    {
        $result = parent::serialize();

        return $result;
    }
}
