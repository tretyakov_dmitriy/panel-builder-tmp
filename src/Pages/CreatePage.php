<?php


namespace Digitech\PanelBuilder\Pages;

use Digitech\PanelBuilder\Tabs\FieldsTab;

/**
 * Class CreatePage
 * @package Digitech\PanelBuilder\Pages
 * @property FieldsTab[] $tabs
 */
class CreatePage extends DetailPage
{
    protected $key = 'create';

    public function __construct()
    {
        parent::__construct("create");
    }
}
