<?php


namespace Digitech\PanelBuilder\WebHooks;

class WebHook
{
    const EVENT_NAVIGATION_END = 'navigation_end';

    private $event, $params, $activationRule;

    public function __construct($event, $url, $data = [])
    {
        $this->event = $event;
        $this->params = [
            'url' => $url,
            'data' => $data,
        ];
        return $this;
    }

    public function serialize() {
        return [
            'event' => $this->event,
            'params' => $this->params
        ];
    }
}
