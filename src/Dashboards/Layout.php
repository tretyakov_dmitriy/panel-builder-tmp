<?php

namespace Digitech\PanelBuilder\Dashboards;

class Layout implements \JsonSerializable
{
    private $main_widgets = [], $left_widgets = null, $right_widgets = null, $available_widgets = [], $editable = false, $id;

    private function __construct()
    {
    }

    static function make(array $widgets = [])
    {
        $model = new static();
        $model->main_widgets = $widgets;
        return $model;
    }

    function withLeftSidebar(array $widgets = [])
    {
        $this->left_widgets = $widgets;
        return $this;
    }

    function withRightSidebar(array $widgets = [])
    {
        $this->right_widgets = $widgets;
        return $this;
    }

    function editable(string $id, array $available_widgets = []) {
        $this->editable = true;
        $this->id = $id;
        $result = [];
        foreach($available_widgets as $widget) {
            if($widget->authorize()) {
                $result[] = $widget;
            }
        }
        $this->available_widgets = $result;
        return $this;
    }

    function jsonSerialize()
    {
        $main_widgets = [];
        foreach($this->main_widgets as $widget) {
            if($widget->authorize()) {
                $main_widgets[] = $widget->serialize();
            }
        }

        if($this->left_widgets !== null) {
            $left_widgets = [];
            foreach($this->left_widgets as $widget) {
                if($widget->authorize()) {
                    $left_widgets[] = $widget->serialize();
                }
            }
        } else {
            $left_widgets = null;
        }

        if($this->right_widgets !== null) {
            $right_widgets = [];
            foreach($this->right_widgets as $widget) {
                if($widget->authorize()) {
                    $right_widgets[] = $widget->serialize();
                }
            }
        } else {
            $right_widgets = null;
        }

        $available_widgets = [];
        foreach($this->available_widgets as $widget) {
            $available_widgets[] = $widget->serialize();
        }

        return [
            'id' => $this->id,
            'widgets' => [
                'center' => $main_widgets,
                'left' => $left_widgets,
                'right' => $right_widgets,
                'available' => $available_widgets,
            ],
            'editable' => $this->editable
        ];
    }
}
