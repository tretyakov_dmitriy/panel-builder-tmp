<?php

namespace Digitech\PanelBuilder\Dashboards;

use Digitech\PanelBuilder\StandaloneEntity;

abstract class Dashboard extends StandaloneEntity
{

    public static function type()
    {
        return 'dashboard';
    }

    static function logo($params)
    {
        return null;
    }

    abstract static function name($params);

    abstract static function layouts($params);

    static function serialize($params = [])
    {
        return array_merge(
            parent::serialize($params), [
            'cache' => false, // У дашбоардов отключено кеширование, позже можно сделать
            'logo' => static::logo($params),
            'name' => static::name($params),
            'layouts' => static::layouts($params)
        ]);
    }
}
