<?php


namespace Digitech\PanelBuilder\Traits;


trait HasDependencies
{
    public $dependencies = [];

    function dependencies($dependencies) {
        $this->dependencies = $dependencies;
        return $this;
    }

    /**
     * @param $attribute
     * @param $value
     * @return $this
     * @deprecated
     */
    function dependsOn($attribute, $value) {
        $this->dependencies[] = [
            'attribute' => $attribute,
            'type' => 'equal',
            'value' => $value
        ];
        return $this;
    }

    /**
     * @param $attribute
     * @return $this
     * @deprecated
     */
    function dependsOnEmpty($attribute) {
        $this->dependencies[] = [
            'attribute' => $attribute,
            'type' => 'empty'
        ];
        return $this;
    }

    /**
     * @param $attribute
     * @return $this
     * @deprecated
     */
    function dependsOnNotEmpty($attribute) {
        $this->dependencies[] = [
            'attribute' => $attribute,
            'type' => 'not-empty'
        ];
        return $this;
    }

//    /**
//     * @param $dependency
//     * @return $this
//     * @deprecated
//     */
//    function dependsOnRaw($dependency) {
//        $this->dependencies[] = $dependency;
//        return $this;
//    }
}
