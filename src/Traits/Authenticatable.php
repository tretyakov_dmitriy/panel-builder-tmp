<?php

namespace Digitech\PanelBuilder\Traits;

trait Authenticatable
{

    static function availableReplacements()
    {
        return [];
    }

    static function activeReplacement()
    {
        return null;
    }

    static function availableScopes()
    {
        return [];
    }

    abstract static function user();
}
