<?php


namespace Digitech\PanelBuilder\Traits;


use Digitech\PanelBuilder\WebHooks\WebHook;

/**
 * Trait HasWebHooks
 * @package Digitech\PanelBuilder\Resource\Traits
 * @property WebHook[] $webHooks Список веб хуков
 */
trait HasWebHooks
{
    public $webHooks = [];
}
