<?php


namespace Digitech\PanelBuilder\Traits;

use Digitech\PanelBuilder\Actions\Action;
use Digitech\PanelBuilder\Actions\Buttons\SubmitButton;

trait HasActions
{
    /**
     * @return Action[]
     */
    static function actions() {
        return [];
    }

    static function serializeActions(array $actions, &$result = []) {
        foreach ($actions as $label => $element) {
            if(is_array($element)) {
                $result[] = [
                    'type' => 'folder',
                    'name' => $label,
                    'children' => self::serializeActions($element)
                ];
            } else {
                $result[] = $element::serialize(is_string($label) ? $label : null);
            }
        }
        return $result;
    }
}
