<?php


namespace Digitech\PanelBuilder\Traits;

use Digitech\PanelBuilder\Fields\Field;

/**
 * Trait HasFields
 * @package Digitech\PanelBuilder\Resource\Traits
 * @property Field[] $fields
 */
trait HasFields
{
    public $fields = [];
}
