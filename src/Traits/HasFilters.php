<?php


namespace Digitech\PanelBuilder\Traits;

use Digitech\PanelBuilder\Fields\Field;

/**
 * Trait HasFilters
 * @package Digitech\PanelBuilder\Resource\Traits
 */
trait HasFilters
{
    /**
     * @return Field[]
     */
    static function filters()
    {
        return [];
    }

    static function serializeFilters()
    {
        $result = [];

        foreach (static::filters() as $filter) {
            if (!is_string($filter)) {
                $result[] = $filter->serialize();
            } else {
                $result[] = $filter;
            }
        }
        return $result;
    }
}
