<?php

namespace Digitech\PanelBuilder\Actions\Buttons;


class SubmitButton extends Button
{
    private $name, $action;
    public function __construct($name, string $action_classname = null)
    {
        $this->name = $name;
        $this->action = $action_classname;
    }

    function getLinkedAction() {
        return $this->action;
    }

    function serialize() {
        return [
            'type' => 'submit',
            'linked_action' => $this->action,
            'name' => $this->name
        ];
    }
}
