<?php


namespace Digitech\PanelBuilder\Actions;

use Digitech\PanelBuilder\Fields\Field;
use Digitech\PanelBuilder\Fields\Hidden;
use Digitech\PanelBuilder\Traits\HasFields;

abstract class RequestAction extends Action
{
    abstract static function url();

    public static function fields() {
        return [];
    }

    static function standalone()
    {
        $result = true;
        if(str_contains(static::url(), ':id') !== false) {
            $result = false;
        }
        if($result === true) {
            foreach (static::fields() as $field) {
                if($field instanceof Hidden) {
                    if(isset($field->getParameters()['default']) && str_contains($field->getParameters()['default'], ':id')) {
                        $result = false;
                        break;
                    }
                }
            }
        }
        return $result;
    }

    public static function authorize($model)
    {
        return true;
    }

    public static function serialize($name_in_list = null)
    {
        $fieldsData = [];
        foreach (static::fields() as $field) {
            if(!is_string($field)) {
                $fieldsData[] = $field->serialize();
            } else {
                $fieldsData[] = $field;
            }
        }
        return array_merge([
            'id' => static::class,
            'name' => static::name(),
            'type' => 'request_action',
            'params' => [
                'url' => static::url(),
                'method' => static::method()
            ],
            'standalone' => static::standalone(),
            'fields' => $fieldsData
        ], parent::serialize($name_in_list));
    }
}
