<?php


namespace Digitech\PanelBuilder\Actions;


class Response
{
    public $refresh = true;
    public $message = null;
    public $report = null;
    public $file_url = null;
    public $file = null;
    public $copy_text_to_clipboard = null;
    public $commands = [];
    static function make()
    {
        return new static();
    }

    function message($message) {
        $this->message = $message;
        return $this;
    }

    function commands($commands) {
        $this->commands = $commands;
        return $this;
    }

    function withoutRefresh() {
        $this->refresh = false;
        return $this;
    }

    function file(string $label, string $filename, string $mime_type, string $content) {
        $this->file = [
            'label' => $label,
            'filename' => $filename,
            'mime_type' => $mime_type,
            'content' => $content,
        ];

        return $this;
    }

    function copyTextToClipboard(string $text) {
        $this->copy_text_to_clipboard = $text;
    }

    function fileUrl(string $label, string $url) {
        $this->file_url = [
            'label' => $label,
            'url' => $url
        ];

        return $this;
    }

    function report(array $data, string $name = null) {
        $this->report = [
            'name' => $name,
            'data' => $data
        ];

        return $this;
    }
}
