<?php


namespace Digitech\PanelBuilder\Actions;

use Digitech\PanelBuilder\Fields\Hidden;

abstract class PostRequestAction extends RequestAction
{
    final static function method() {
        return 'post';
    }

}
