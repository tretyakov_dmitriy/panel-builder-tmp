<?php


namespace Digitech\PanelBuilder\Actions;

abstract class GetRequestAction extends RequestAction
{
    final static function method() {
        return 'get';
    }
}
