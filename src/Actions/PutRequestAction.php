<?php


namespace Digitech\PanelBuilder\Actions;

use Digitech\PanelBuilder\Fields\Hidden;

abstract class PutRequestAction extends RequestAction
{
    final static function method() {
        return 'put';
    }
}
