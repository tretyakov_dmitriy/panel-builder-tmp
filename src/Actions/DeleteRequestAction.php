<?php


namespace Digitech\PanelBuilder\Actions;

abstract class DeleteRequestAction extends RequestAction
{
    final static function method() {
        return 'delete';
    }
}
