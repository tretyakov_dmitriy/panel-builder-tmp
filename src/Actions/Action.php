<?php


namespace Digitech\PanelBuilder\Actions;

use Digitech\PanelBuilder\Actions\Buttons\Button;

/**
 * Class Action
 * @package Digitech\PanelBuilder\Actions
 * @property Button $buttons
 */
abstract class Action
{
    static function confirm() {
        return true;
    }

    abstract static function name();

    abstract static function standalone();

    static function buttons() {
        return [];
    }

    static function serialize($name_in_list = null) {
        $buttons_data = [];
        foreach (static::buttons() as $button) {
            $buttons_data[] = $button->serialize();
        }
        return [
            'buttons' => $buttons_data,
            'name_in_list' => $name_in_list,
            'confirm' => static::confirm()
        ];
    }
}
