<?php

namespace Digitech\PanelBuilder\Views;

class CardView
{
    protected $body_fields,
        $image,
        $header = ['left' => ['icon' => null, 'field' => null], 'right' => ['icon' => null, 'field' => null]],
        $footer = ['left' => ['icon' => null, 'field' => null], 'right' => ['icon' => null, 'field' => null]];

    public function __construct(array $body_fields)
    {
        $this->body_fields = $body_fields;
        return $this;
    }

    /**
     * @param string $attribute
     * @return $this
     */
    public function image($attribute)
    {
        $this->image = $attribute;
        return $this;
    }

    public function headerLeftField($attribute)
    {
        $this->header['left']['field'] = $attribute;
        return $this;
    }

    public function headerLeftIcon($name, $color = 'secondary', $position = 'before')
    {
        $this->header['left']['icon'] = [
            'name' => $name,
            'position' => $position,
            'color' => $color
        ];
        return $this;
    }


    public function headerRightField($attribute)
    {
        $this->header['right']['field'] = $attribute;
        return $this;
    }

    public function headerRightIcon($name, $color = 'secondary', $position = 'before')
    {
        $this->header['right']['icon'] = [
            'name' => $name,
            'position' => $position,
            'color' => $color
        ];
        return $this;
    }

    public function footerLeftField($attribute)
    {
        $this->footer['left']['field'] = $attribute;
        return $this;
    }

    public function footerLeftIcon($name, $color = 'secondary', $position = 'before')
    {
        $this->footer['left']['icon'] = [
            'name' => $name,
            'position' => $position,
            'color' => $color
        ];
        return $this;
    }


    public function footerRightField($attribute)
    {
        $this->footer['right']['field'] = $attribute;
        return $this;
    }

    public function footerRightIcon($name, $color = 'secondary', $position = 'before')
    {
        $this->footer['right']['icon'] = [
            'name' => $name,
            'position' => $position,
            'color' => $color
        ];
        return $this;
    }

    public function serialize()
    {
        return [
            'view' => 'cards',
            'header' => $this->header,
            'body' => $this->body_fields,
            'footer' => $this->footer,
            'image' => $this->image
        ];
    }
}
