<?php
namespace Digitech\PanelBuilder\Views;

class TabularView
{
    public $columns = [];

    /**
     * @param string[] $columns
     */
    public function __construct(array $columns)
    {
        $this->columns = $columns;
    }

    public function serialize() {
        return [
            'view' => 'table',
            'columns' => $this->columns
        ];
    }
}
