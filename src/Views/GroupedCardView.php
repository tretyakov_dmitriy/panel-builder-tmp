<?php

namespace Digitech\PanelBuilder\Views;

class GroupedCardView extends CardView
{
    private $group_by = [];
    public function __construct(array $body_fields, $group_by)
    {
        $this->body_fields = $body_fields;
        $this->group_by = $group_by;
        return $this;
    }

    public function serialize()
    {
        return array_merge(
            parent::serialize(),
            [
                'view' => 'grouped-cards',
                'group_by' => $this->group_by
            ]
        );
    }
}
